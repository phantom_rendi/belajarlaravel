<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PageController extends Controller
{
    public function about(){

        $name = "Rendi";

        return view('about', compact('name'));

    }


    public function contact(){

        $no = '089288000';

        return view('contact', compact('no'));
    }

    public function other(){
        $hobby = 'Main Bola';

        return view('other', ['hobbies'=> $hobby]);
    }
}
