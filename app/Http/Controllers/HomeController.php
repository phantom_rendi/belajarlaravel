<?php

namespace App\Http\Controllers;
use DB;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){

        $taks = DB::table('taks')->get();
       
        return view('welcome',['taks' => $taks]);
    }
}
